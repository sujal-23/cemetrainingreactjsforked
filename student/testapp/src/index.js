import React from "react";
import ReactDOM from "react-dom";
import "./index.css";

// const header = React.createElement("h1", null, "My Header");

// const test = React.createElement(
//   "p",
//   { className: "hello-world", id: "hello-id" },
//   "Hello World !!!"
// );

// const main = React.createElement("div", { className: "hello-container" }, [
//   header,
//   test,
// ]);

// ReactDOM.render(main, document.getElementById("root"));

const main = (
  <section>
    <div>
      <h1>Ice Cream Shop</h1>
    </div>
    <div id="ice-cream">
      <h2>Ice Cream Flavours</h2>
      <ul className="ice-cream-list">
        <li>Vanilla</li>
        <li>Chocolate</li>
        <li className="Raspberry-color">Raspberry Ripple</li>
        <li>Cookies & Cream</li>
      </ul>
    </div>
  </section>
);

ReactDOM.render(main, document.getElementById("root"));
