import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Provider } from "react-redux";
import { createStore, combineReducers } from "redux";

const weightReducer = (state = 100, action) => {
  //   if (state === undefined) {
  //     state = 200;
  //   }
  return state;
};

const heightReducer = (state = 200, action) => {
  //   if (state === undefined) {
  //     state = 200;
  //   }
  return state;
};

const countReducer = (state = 0, action) => {
  //   if (state === undefined) {
  //     state = 1;
  //   }

  if (action.type === "ADD_COUNT") {
    return action.payload;
  }
  if (action.type === "DECREMENT_COUNT") {
    return state - 1;
  }
  return state;
};

const reducers = combineReducers({
  count: countReducer,
  weight: weightReducer,
  height: heightReducer,
});

const store = createStore(reducers);
console.log("store is ", store.getState());

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
