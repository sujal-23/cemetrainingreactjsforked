import React from "react";
import { connect } from "react-redux";

const Height = (props) => {
  return (
    <>
      <div>Height Component</div>;<p> height is {props.height}</p>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    count: state.count,
    weight: state.weight,
    height: state.height,
  };
};

export default connect(mapStateToProps)(Height);
