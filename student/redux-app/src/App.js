import React from "react";
import { connect } from "react-redux";
import { addCount, decrementCount } from "./actions";
import Height from "./Height";

const App = (props) => {
  return (
    <>
      <div> App Component</div>
      <p>
        count is {props.count}{" "}
        <button
          onClick={() => {
            props.addCount(props.count + 1);
          }}
        >
          {" "}
          Add Count{" "}
        </button>
      </p>
      <p>weight is {props.weight}</p>
      <Height />
    </>
  );
};

const actionCreators = {
  addCount,
  decrementCount,
};

const mapStateToProps = (state) => {
  return {
    count: state.count,
    weight: state.weight,
    height: state.height,
  };
};

export default connect(mapStateToProps, actionCreators)(App);
