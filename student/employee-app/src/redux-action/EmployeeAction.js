
import constants from  '../common/constants';
import axios from "axios";

export const getEmployeeListBegin= () =>{

    return{
        type: constants.GET_EMPLOYEE_LIST_BEGIN,
    }

};

export const getEmployeeListSuccess = (employeeList) =>{

    return{
        type: constants.GET_EMPLOYEE_LIST_SUCCESS,
        payload: employeeList,
    }

};

export const getEmployeeListFailure = (error) =>{

    return{
        type: constants.GET_EMPLOYEE_FAILURE,
        payload:{ message: "Error Occured while processing"} ,
    }
};

export const getEmployeeList = () =>{
console.log("logggg ",constants.GET_EMPLOYEE_FAILURE);
    return (dispatch, getState) => {
        dispatch(getEmployeeListBegin());
        axios.get("http://localhost:8080/api/all").then((res) => {
            console.log("in action, ",res.data);
            setTimeout(() => {

           dispatch(getEmployeeListSuccess(res.data));
            }, 4000);
          },
          (error) => {
              dispatch(getEmployeeListFailure(error));
          });
    }
};

export const addEmployeeFailure = (error) =>
{
return {
    type : constants.ADD_EMPLOYEE_FAILURE,
    payload : { message: "error while adding an employee record."},
}
}

export const addEmployeeSuccess = () =>
{
return {
    type : constants.ADD_EMPLOYEE_SUCCESS,
    
}
}

export const addEmployee = (employee) => {
    return (dispatch, getState) => {
        return axios.post("http://localhost:8080/api/save", employee).then((res) => {
        console.log("created");
        dispatch(addEmployeeSuccess());
    },
    (err) => {
dispatch(addEmployeeFailure(err));
    });
    }
}