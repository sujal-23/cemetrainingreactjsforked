import React from "react";
import ReactDom from "react-dom";
import App from "./components/App";
import { BrowserRouter as Router } from "react-router-dom";
import "./style/AppStyle.css";
import { Provider} from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from "./redux-reducer";
//import rootReducer from "../redux-reducer/index";


const store = createStore(rootReducer, applyMiddleware(thunkMiddleware));

ReactDom.render(
  <Provider store = {store}>
  <Router>
    <App />
  </Router>
  </Provider>,
  document.getElementById("root")
);
