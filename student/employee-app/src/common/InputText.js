import React from "react";

const InputText = ({ type, name, textValue, title, onChange }) => {
  return (
    <div className="form-group col-md-5">
      <label style={{ width: "100px" }}>{title}</label>
      <input type={type} id={name} value={textValue} onChange={onChange} />
    </div>
  );
};

export default InputText;
