import React, { useState, useEffect } from "react";
import EmployeeListing from "./EmployeeListing";
import Banner from "../common/Banner";
import Header from "../common/Header";
import AddEmployee from "./AddEmployee";
import { Switch, Route } from "react-router-dom";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { connect} from 'react-redux';
import {getEmployeeList} from '../redux-action/EmployeeAction'

const App = ( { getEmployeeList}) => {
  //const [empList, setEmpList] = useState([]);

  useEffect(() => {
    getEmployeeList();
  }, [getEmployeeList]);

  return (
    <div className="app">
      <Header />
      <Banner />
      <ToastContainer />
      <Switch>
        <Route exact path="/">
         
         <EmployeeListing />
        </Route>
        <Route path="/addEmployee">
          <AddEmployee />
        </Route>
      </Switch>
    </div>
  );
};



export default connect(null, { getEmployeeList }) (App);

// [
//   {
//     id: 1,
//     name: "Spring Boot",
//     age: 20,
//     email: "spring@test.com",
//     salary: 575,
//   },
//   {
//     id: 2,
//     name: "John test",
//     age: 40,
//     email: "joh.test@test.com",
//     salary: 3575,
//   },
//   {
//     id: 3,
//     name: "Danny Test",
//     age: 30,
//     email: "danny@test.com",
//     salary: 5875,
//   },
// ];
