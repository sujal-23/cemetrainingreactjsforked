import React, { useState } from "react";
import InputText from "../common/InputText";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { toast } from 'react-toastify';
import { connect, useDispatch } from "react-redux";
import { addEmployee } from "../redux-action/EmployeeAction";

const AddEmployee = () => {
  const [employee, setEmployee] = useState({
    id: null,
    age: "",
    name: "",
    salary: "",
    email: "",
  });
  //const [age, setAge] = useState("");
  const dispatch = useDispatch();
  const history = new useHistory();
  const handleChange = ({ target }) => {
    setEmployee({
      ...employee,
      [target.id]: target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // axios.post("http://localhost:8080/api/save", employee).then((res) => {
    //   console.log("created");
    //   toast.success("Employee created succesfully !");
    //   history.push("/");
    // });
    //console.log("save");
    //console.log(employee);

    dispatch(
			addEmployee(employee)
		)
			.then(() => {
				console.log("add Employee is successful");
			})
			.catch(() => {})
			.finally(() => {
        console.log("addAlbum thunk function is completed");
        toast.success("Employee created succesfully !");
				history.push("/");
			});

  };

  return (
    <div className="container">
      <h3> Add New album </h3>
      <form onSubmit={handleSubmit}>
        <div className="form-row">
          <InputText
            type="number"
            name="id"
            title="id"
            value={employee.id}
            onChange={handleChange}
          />
          <InputText
            type="text"
            name="name"
            title="Name"
            value={employee.name}
            onChange={handleChange}
          />
          <InputText
            type="number"
            name="age"
            title="Age"
            value={employee.age}
            onChange={handleChange}
          />
          <InputText
            type="number"
            name="salary"
            title="Salary"
            value={employee.salary}
            onChange={handleChange}
          />
          <InputText
            type="email"
            name="email"
            title="Email"
            value={employee.email}
            onChange={handleChange}
          />
        </div>
        <div className="form-group" style={{ textAlign: "center" }}>
          <input type="submit" value="Create Employee" />
        </div>
      </form>
    </div>
  );
};

export default AddEmployee;
