import React from "react";
import Employee from "./Employee";
import { connect, useSelector } from "react-redux";

const EmployeeListing = () => {
	const empList = useSelector((state) => state.employeeList.employeeEntity);
	const error = useSelector((state) => state.employeeList.errors);
	const loading = useSelector((state) => state.employeeList.loading);
 console.log("loading....." , loading);
	if (error) {
		return <div className="d-flex justify-content-center">{error.message}</div>;
	}

	if (loading) {
		return (
			<div className="d-flex justify-content-center">
				<div
					className="spinner-border m-5"
					style={{ width: "4rem", height: "4rem" }}
					role="status"
				>
					<span className="sr-only">Loading...</span>
				</div>
			</div>
		);
	}

	return (
        <div className="container">
        <div className="row">
          {empList.map((emp) => (
            <Employee key={emp.id} name={emp.name} emp={emp} />
          ))}
        </div>
      </div>
	);
};

// refactor below to useSelector
// const mapStateToProps = (state) => {
//     console.log(state);
//    return { 
   
//      empList: state.employeeList.employeeEntity,
//      error: state.errors,
//      loading: state.employeeList.loading,
//    }
//   };

// export default connect(mapStateToProps) (EmployeeListing);;

export default EmployeeListing;
