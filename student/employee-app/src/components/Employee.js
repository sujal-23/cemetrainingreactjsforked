import React, { useState } from "react";
import EmployeeHeader from "./EmployeeHeader";
import EmployeeList from "./EmployeeList";
import EmployeeToggle from "./EmployeeToggle";

const Employee = ({ name, emp }) => {
  const [visible, setVisibility] = useState(true);

  return (
    <div className="col-md-4">
      <div
        className="card mb-4 box-shadow"
        style={{ width: "20rem", wordBreak: "break-all" }}
      >
        <div className="card-body">
          <EmployeeHeader name={name} />
          <EmployeeList empdetail={emp} visible={visible} />
          <EmployeeToggle toggle={setVisibility} visible={visible} />
        </div>
      </div>
    </div>
  );
};

export default Employee;
