import React from "react";

const EmployeeList = (props) => {
 
  return (
    props.visible && (
      <ol
        className="album-tracks"
        style={{ width: "20rem", wordBreak: "break-all" }}
      >
        <li >
          {" "}
          <b>ID :</b> {props.empdetail.id}
        </li>
        <li >
          {" "}
          <b>Age : </b>
          {props.empdetail.age}
        </li>
        <li >
          {" "}
          <b>Emails :</b> {props.empdetail.email}
        </li>
        <li >
          {" "}
          <b>Salary :</b> {props.empdetail.salary}
        </li>
      </ol>
    )
  );
};

export default EmployeeList;
