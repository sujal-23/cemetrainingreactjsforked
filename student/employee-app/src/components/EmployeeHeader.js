import React from "react";

const EmployeeHeader = ({ name }) => {
  return (
    <div>
      <h6 className="album-artist card-subtitle mb-2 text-muted"> {name}</h6>
    </div>
  );
};

export default EmployeeHeader;
