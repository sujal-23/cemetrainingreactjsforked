import { combineReducers } from 'redux';
import EmployeeReducer from './Employee-Reducer';
import EmployeeReducerFailure from './Employee-Reducer-Failure';


const rootReducer = combineReducers({
    employeeList : EmployeeReducer,
    errors: EmployeeReducerFailure
});

export default rootReducer;