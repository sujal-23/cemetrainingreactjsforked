import constants from '../common/constants'
const initialState = { employeeEntity: []}

export default (state = initialState, action) => {
    switch(action.type){
        case constants.GET_EMPLOYEE_LIST_BEGIN:
            return {...state, loading: true, error: null};

        case constants.GET_EMPLOYEE_LIST_SUCCESS:
            return {...state, employeeEntity: action.payload, loading: false};

        case constants.GET_EMPLOYEE_FAILURE:
            return {...state, employeeEntity: [], loading: false, error:action.payload};

        case constants.ADD_EMPLOYEE_SUCCESS:
                return {...state, loading:false, error: null};
        
        case constants.ADD_EMPLOYEE_FAILURE:
                    return {...state, loading:false, error: action.payload};
        
            default:
            return state;

    }
}