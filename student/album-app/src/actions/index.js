export const fetchAlbums = (albums) => {
  return {
    type: "FETCH_ALBUMS_SUCCESS",
    payload: albums,
  };
};
