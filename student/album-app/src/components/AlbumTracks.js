import React from "react";

const AlbumTracks = (props) => {
  return (
    props.visible && <p>{props.tracks} tracks</p>
    // props.visible && (<ol className="album-tracks">
    //   {props.tracks.map((track) => {
    //     return <li key={track}>{track}</li>;
    //   })}
    // </ol>)
  );
};

export default AlbumTracks;
