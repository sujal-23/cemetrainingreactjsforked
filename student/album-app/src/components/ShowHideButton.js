import React from "react";

const ShowHideButton = (props) => {
  return (
    <button
      className="btn btn-sm btn-outline-secondary"
      onClick={() => props.toggle(!props.visible)}
    >
      {" "}
      Toggle
    </button>
  );
};

export default ShowHideButton;
