import React, { useState, useEffect } from "react";
import Album from "./Album";
import axios from "axios";
import Banner from "./Banner";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import CreateAlbumForm from "./CreateAlbumForm";
import { connect } from "react-redux";
import { fetchAlbums } from "../actions/index";

const App = (props) => {
  //const [albums, setAlbums] = useState([]);

  useEffect(() => {
    axios.get("http://localhost:8088/albums").then((res) => {
      //console.log(res);
      //setAlbums(res.data);
      props.fetchAlbums(res.data);
    });
  }, [props.fetchAlbums]);

  return (
    <Router>
      <div className="app">
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/add">Add Album</Link>
            </li>
          </ul>
        </nav>
        <Banner />

        <Switch>
          <Route exact path="/add">
            <CreateAlbumForm />
          </Route>
          <Route path="/">
            <h1> Home Page </h1>
            <div className="container">
              <div className="row">
                {props.albums.map((album) => {
                  return (
                    <Album
                      key={album.title}
                      title={album.title}
                      artist={album.artist}
                      tracks={album.tracks}
                    />
                  );
                })}
              </div>
            </div>
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

const mapStatetoProps = (state) => {
  return {
    albums: state.albums,
  };
};

const actionCreators = {
  fetchAlbums,
};

export default connect(mapStatetoProps, actionCreators)(App);

// {
//   title: "Album 1",
//   artist: "artist 1",
//   tracks: ["Track 1", "track 2", "Track 3"],
// },
// {
//   title: "Album 2",
//   artist: "artist 2",
//   tracks: ["Track 4", "track 5", "Track 6"],
// },
// {
//   title: "Album 3",
//   artist: "artist 3",
//   tracks: ["Track 7", "track 8", "Track 9"],
// },
// ];
