import React from "react";

const AlbumTitle = (props) => {
  return <h5 className="album-title card-title">{props.title}</h5>;
};

export default AlbumTitle;
