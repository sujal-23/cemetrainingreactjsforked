import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";

import { Provider } from "react-redux";
import { createStore } from "redux";

const albumsReducer = (state = { albums: [] }, action) => {
  switch (action.type) {
    case "FETCH_ALBUMS_SUCCESS":
      return { ...state, albums: action.payload };
    default:
      return state;
  }

  //return state;
};

const store = createStore(albumsReducer);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
