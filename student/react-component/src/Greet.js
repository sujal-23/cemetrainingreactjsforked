import React from "react";

class Greet extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: props.name,
    };
  }
  render() {
    return <h1> Hello {this.state.name}</h1>;
  }
}

export default Greet;

// function Greet({ name }) {
//   console.log(name);
//   return <h1>{name}</h1>;
// }
