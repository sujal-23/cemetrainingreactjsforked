import React, { useState } from "react";
import ReactDOM from "react-dom";
import Person from "./Person";
import Greet from "./Greet";

const PersonFunc = ({ name }) => {
  const [height, setHeight] = useState(185);
  const [weight, setWeight] = useState(200);

  return (
    <div>
      <h5>
        <Greet name={name} />
      </h5>
      <p>
        {name} weighs {weight} and {height} tall.
      </p>

      <button onClick={() => setWeight(weight + 1)}>Grow weight</button>
      <button onClick={() => setHeight(height + 1)}>Grow Height</button>
    </div>
  );
};

const main = (
  <>
    <Person name="John" />
    <PersonFunc name="Harry" />
  </>
);

ReactDOM.render(main, document.getElementById("root"));
