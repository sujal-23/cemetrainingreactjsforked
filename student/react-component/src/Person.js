import React from "react";
import Greet from "./Greet";

class Person extends React.Component {
  constructor(props) {
    super(props);

    this.state = { height: 185, weight: 200 };
    //this.grow = this.grow.bind(this);
  }

  //   grow() {
  //     this.setState({ weight: "300" });
  //   }

  growWeight = () => {
    this.setState({ weight: this.state.weight + 5 });
  };
  growHeight = () => {
    this.setState((state) => {
      return {
        height: state.height + 1,
      };
    });
  };

  render() {
    return (
      <div>
        <h5>
          <Greet name={this.props.name} />
        </h5>
        <p>
          {this.props.name} weighs {this.state.weight} and {this.state.height}{" "}
          tall.
        </p>

        <button onClick={this.growWeight}>Grow weight</button>
        <button onClick={this.growHeight}>Grow Height</button>
      </div>
    );
  }
}

export default Person;
